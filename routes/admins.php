<?php

use App\Http\Controllers\Auth\AdminLoginController;
use App\Http\Controllers\Auth\AdminRegisterController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('admin-login', [AdminLoginController::class,'adminLogin']);
Route::post('admin-login', ['as'=>'admin-login','uses'=>'App\Http\Controllers\Auth\AdminLoginController@adminLoginPost']);
Route::get('admin-register', function(){
    return view('admin.adminRegister');
});

Route::post('admin-register', [AdminLoginController::class,'adminRegister'])->name('admin-register');



