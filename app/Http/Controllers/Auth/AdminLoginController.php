<?php

namespace App\Http\Controllers\Auth;

use App\Models\Admin;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AdminRegisterController;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function adminLogin()
    {
        return view('admin.adminLogin');
    }


    public function adminLoginPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if (auth()->guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $user = auth()->guard('admin')->user();
            // dd($user);
            return view('home')->with('user',$user->name);
        } else {
            return back()->with('error', 'your username and password are wrong.');
        }
    }
    public function adminRegister(Request $request)
    {
        $register = new AdminRegisterController();
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $data =
            [
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
            ];
        $register->create($data);
        return view('home');

    }
}
